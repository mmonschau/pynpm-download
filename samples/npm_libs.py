from pynpmd import JsLibResolver

if __name__ == "__main__":
    libs = ["d3", "@hpcc-js/wasm", "d3-graphviz"]
    rsvr = JsLibResolver()
    for lib in libs:
        print(rsvr.get_lib(lib))
    rsvr.flush()
